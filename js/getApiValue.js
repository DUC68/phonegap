$( document ).ready(function() {
    console.log( "ready!" );    
    $.ajax({
        url: "http://de-moor.vincent.formations-web.alsace/webservice-prestashop/proxy_prestashop.php",
        type: "GET",
        data: {
            action:"tree"
        }})
        .done(function(response) {
            console.log(response);

            //stockage en local
            $.each(response.categories, function(key, category) {

                if(category.id_parent == 0){
                    localStorage.setItem("id_main_category", category.id);
                }

                localStorage.setItem("cat_"+category.id, JSON.stringify(category));
                var name =JSON.parse(localStorage.getItem("cat_"+category.id)).name;    
            });

            //lacement du rafraichissement de la page
            drawMenu();
            //...
            //var storedNames = JSON.parse(localStorage.getItem("names"));

        }) 

        function drawMenu()
        {
            //trouver la catégorie de départ
            var id_main_category = localStorage.getItem('id_main_category'); //@TODO rendre dynamique cet ID
            var category_racine = JSON.parse(localStorage.getItem('cat_' + id_main_category));

            //console.log(id_main_category);
            //console.log(category_racine);

            //puis appeler de quoi la dessiner
            $.each(category_racine.subcategories,function(i,id_sub_category){
                dessinerNoeux(id_sub_category, $('#menu')); 
            });

            $("#my-menu").mmenu({
                
            });
            var API = $("#my-menu").data( "mmenu" );
            // $("#my-button").click(function() {
            //   API.open();
            //   API.close();
            // });

            //   Trigger a method
            API.openPanel( $("#my-panel") );

        }

        function dessinerNoeux(id_category, $container) {

            var category = JSON.parse(localStorage.getItem('cat_' + id_category));

            if(category !== false)
            {
                var favorite = '';
                
                if(localStorage.getItem('fav_' + id_category) != null)
                {
                    favorite = ' favorite';
                }
                console.log(['Je dessine : ', category]);
                var $li = $("<li><a>"+category.name+"<i class='fas fa-star'"+favorite+"'></i></a></li>").click(function(e){
                    drawProducts(category.id);
                    //e.preventDefault();
                });
                $('i.fas', $li).click(function(e){
                    e.preventDefault();
                    _toggleFavorite(id_category, $(this).hasClass('favorite'));
                    $(this).toggleClass('favorite');
                });
            


                var $ul = $('<ul></ul>');

                $li.append($ul);
                $container.append($li);
                console.log('***********');
                console.log($('#menu').html());
                //si j'ai des enfants alors
                //boucle sur les enfants et appel de "dessinerNoeux pour chaque enfant"
                $.each(category.subcategories,function(i,id_sub_category){

                    console.log(id_sub_category)
                    dessinerNoeux(id_sub_category, $ul); 
                });
            }
            


        }

        var _toggleFavorite = function(id_category, is_favorite)
        {
            if(is_favorite)
            {
                localStorage.removeItem('fav_' + id_category);
                
                //vider localstorage des produits de cette categorie
                
                var category = _getCategoryFromLocalStorage(id_category);
                    
                $.each(category.products,function(i,id_product){
                    //finition, il faudrait tester si ce produit n'est pas présent dans une autre catégorie encore en favori
                    localStorage.removeItem('prod_' + id_product);
                });
            }
            else
            {
                if(navigator.onLine === true) //je suis en ligne
                {
                    localStorage.setItem('fav_' + id_category, 1);
                    storeInCacheProductsByIdCategory(id_category);
                }
                else
                {
                    alert('Désolé, vous devez être connecté pour mettre en favori cette catégorie');
                }
                
                
            }
        };

        var storeInCacheProductsByIdCategory = function(id_category) {
            var products = loadProductsByIdCategory(id_category);
            
            $.each(products,function(i,product) {
                
                localStorage.setItem('prod_' + product.id, JSON.stringify(product));
                
            });
            
        };

        var loadProductsByIdCategory = function(id_category)
        {
            console.log('loadProductsByIdCategory('+id_category+')');
            
            var products;
            
            $.ajax({
                url : 'http://de-moor.vincent.formations-web.alsace/webservice-prestashop/proxy_prestashop.php',
                data : {
                    action : 'productByCategoryId',
                    id_categorie : id_category
                },
                async: false,	
            }).done(function(response){
                console.log(response);
                products = response.products;
                
                
            }).fail(function(){
                alert('Proxy injoignable');
            });
            
            return products;
            
        };

        var _getCategoryFromLocalStorage = function(id_category)
        {
            var category = localStorage.getItem('cat_' + id_category);
            
            if(category == null)
            {
                return false;
            }
            else
            {
                return JSON.parse(category);
            }
        };

        	var drawProducts = function(id_category) {
		
                var products = [];
                
                if(navigator.onLine) //je suis en ligne
                {
                    products = loadProductsByIdCategory(id_category);

                }
                else //pas de connection
                {
                    //est-ce une category en favori?
                    if(localStorage.getItem('fav_' + id_category) != null)
                    {
                        //en favori
                        var category = _getCategoryFromLocalStorage(id_category);
                        
                        $.each(category.products,function(i,id_product){
                            var product = localStorage.getItem('prod_' + id_product);
                            
                            if(product != null)
                            {
                                products.push(JSON.parse(product));
                            }
                            
                            
                        });
                        
                    }
                    else
                    {
                        //pas en favori
                        products = false;
                    }
                }
                
                //ici faudra réaliser la génération de l'html
                $('#my-content').html(generateProductListHTML(products));
                
            };
	
	var generateProductListHTML = function(products) {
		
		if(products === false)
		{
			return '<div class="error">Impossible d\'afficher le contenu sans conexion à internet</div>'; 
		}
		
		var html = '<ul>';
		
		console.log(products);
		
			$.each(products,function(i,product){
				if(product == null)
				{
					return;
				}
				html += '<li>';
				html += '<h2>' + product.name + '</h2>';
				html += '<span>' + product.price + '€</span>';
				html += '</li>';
			});
		html += '</ul>';
		
		return html;
	};
	
});